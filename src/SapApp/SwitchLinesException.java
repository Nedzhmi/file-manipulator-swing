package SapApp;

import java.io.Serializable;

public class SwitchLinesException extends Exception implements Serializable {
    public static final long serialVersionUID = 1L;
    @Override
    public String getMessage() {
        return "Invalid indexes for lines! Cannot switch lines!";
    }
}
