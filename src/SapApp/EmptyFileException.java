package SapApp;

import java.io.Serializable;

public class EmptyFileException extends Exception implements Serializable {
    public static final long serialVersionUID = 3L;
    @Override
    public String getMessage() {
        return "ERROR! File is empty!";
    }
}
