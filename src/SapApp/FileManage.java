package SapApp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class FileManage {

    private List<String> lines = new ArrayList<>();

    public void switchWords(String path, int fromLine, int wordIndex1, int toLine, int wordIndex2)
            throws SwitchLinesException, SwitchWordsException, EmptyFileException, IOException, FileNotFoundException {
        lines = readFile(path);
        if (lines.size() == 0) {
            throw new EmptyFileException();
        }

        boolean areLineIndexesValid = validateLinesIndexes(lines, fromLine, toLine);

        if (areLineIndexesValid) {
            String[] firstLine = lines.get(fromLine).split("\\s+");
            String[] secondLine = lines.get(toLine).split("\\s+");

            boolean areWordIndexesValid = validateWordIndexes(firstLine, secondLine, wordIndex1, wordIndex2);

            if (areWordIndexesValid) {
                String firstWordToSwap = firstLine[wordIndex1];
                String secondWordToSwap = secondLine[wordIndex2];

                String temp = firstWordToSwap;
                firstWordToSwap = secondWordToSwap;
                secondWordToSwap = temp;

                firstLine[wordIndex1] = firstWordToSwap;
                String resultFirstLine = String.join(" ", firstLine);
                lines.set(fromLine, resultFirstLine);

                secondLine[wordIndex2] = secondWordToSwap;
                String resultSecondLine = String.join(" ", secondLine);
                lines.set(toLine, resultSecondLine);

                writeToFile(path);
            } else {
                throw new SwitchWordsException();
            }
        } else {
            throw new SwitchLinesException();
        }

    }

    public void switchLines(String path, int fromLine, int toLine)
            throws SwitchLinesException, EmptyFileException, IOException, FileNotFoundException {
        lines = readFile(path);
        if (lines.size() == 0) {
            throw new EmptyFileException();
        }
        boolean areLineIndexesValid = validateLinesIndexes(lines, fromLine, toLine);

        if (areLineIndexesValid) {
            Collections.swap(lines, fromLine, toLine);
            writeToFile(path);
        } else {
            throw new SwitchLinesException();
        }
    }

    private boolean validateLinesIndexes(List<String> lines, int firstIndex, int secondIndex) {
        return (firstIndex >= 0 && firstIndex < lines.size()) && (secondIndex >= 0 && secondIndex < lines.size());
    }

    private boolean validateWordIndexes(String[] firstLine, String[] secondLine, int firstLineWordIndex, int secondLineWordIndex) {
        return ((firstLineWordIndex >= 0 && firstLineWordIndex < firstLine.length)
                && (secondLineWordIndex >= 0 && secondLineWordIndex < secondLine.length));
    }

    private List<String> readFile(String path) throws IOException {
        Path filePath = Path.of(path);
        if (Files.exists(filePath) && !Files.isDirectory(filePath)) {
            try {
                lines = Files.readAllLines(filePath);
            } catch (IOException e) {
                throw new IOException();
            }
        }

        return lines;
    }

    private void writeToFile(String path) throws FileNotFoundException{
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(path);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        }
        for (String line : lines) {
            writer.println(line);
        }
        writer.close();
    }
}
