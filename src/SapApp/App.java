package SapApp;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class App {
    private JTextField swapLineTextField;
    private JTextField withLineTextField;
    private JButton swapLinesButton;
    private JTextField indexOfLine1TextField;
    private JTextField indexOfWord1TextField;
    private JTextField indexOfLine2TextField;
    private JTextField indexOfWord2TextField;
    private JButton loadFileButton;
    private JButton swapWordsButton;
    private JLabel chooseFileLabel;
    private JLabel swapLineLabel;
    private JLabel withLineLabel;
    private JLabel indexOfLine1Label;
    private JLabel indexOfWord1Label;
    private JLabel indexOfLine2Label;
    private JLabel indexOfWord2Label;
    private JPanel panelMain;
    private JFileChooser fileChooser;
    private JLabel filePath;
    private JLabel actionStatus;
    private String path;
    private JFrame frame;
    private FileManage fileManage;
    final private String notValidFileMsg = "Invalid file type! Expected to be .txt!";

    public JPanel getPanelMain() {
        return panelMain;
    }

    public App() {

        swapLineTextField.setInputVerifier(new IntegerInputVerifier());
        withLineTextField.setInputVerifier(new IntegerInputVerifier());

        indexOfLine1TextField.setInputVerifier(new IntegerInputVerifier());
        indexOfWord1TextField.setInputVerifier(new IntegerInputVerifier());
        indexOfLine2TextField.setInputVerifier(new IntegerInputVerifier());
        indexOfWord2TextField.setInputVerifier(new IntegerInputVerifier());

        fileManage = new FileManage();
        loadFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionStatus.setText("");
                fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("."));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files (.txt)", "txt");
                fileChooser.setFileFilter(filter);
                int fileDialogStatus = fileChooser.showOpenDialog(null);
                if (fileDialogStatus == JFileChooser.APPROVE_OPTION) {
                    path = fileChooser.getSelectedFile().getAbsolutePath();

                    String[] tokens = path.split("\\.");
                    String extension = tokens[1];
                    if ("txt".equalsIgnoreCase(extension)) {
                        filePath.setText(path);
                    } else {
                        filePath.setText(notValidFileMsg);
                    }
                }
            }
        });

        swapLinesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionStatus.setText("");

                if (filePath.getText().equals(notValidFileMsg)) {
                    JOptionPane.showMessageDialog(frame,
                            "You have selected a different type of file. Please select a .txt file!",
                            "ERROR: NOT A .TXT FILE",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                try {
                    int fromLine = Integer.parseInt(swapLineTextField.getText());
                    int toLine = Integer.parseInt(withLineTextField.getText());
                    fileManage.switchLines(path, fromLine, toLine);
                    JOptionPane.showMessageDialog(frame,
                            String.format("The file was successfully saved! Changed line %d with line %d!", fromLine, toLine),
                            "CHANGED LINES", JOptionPane.INFORMATION_MESSAGE);
                    actionStatus.setText("The file was successfully saved!");
                } catch (FileNotFoundException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: FILE NOT FOUND!", JOptionPane.ERROR_MESSAGE);
                }  catch (EmptyFileException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: FILE IS EMPTY!", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException exception) {
                    JOptionPane.showMessageDialog(frame,
                            "File not loaded!", "ERROR: FILE NOT LOADED", JOptionPane.ERROR_MESSAGE);
                } catch (SwitchLinesException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: SWAP LINES!", JOptionPane.ERROR_MESSAGE);
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(frame,
                            "You should enter integer value! " + exception.getMessage(),
                            "ERROR: INVALID INPUT", JOptionPane.ERROR_MESSAGE);
                } catch (IOException exception) {
                    JOptionPane.showMessageDialog(frame, exception.getMessage(),
                            "ERROR: IOException", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        swapWordsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionStatus.setText("");

                if (filePath.getText().equals(notValidFileMsg)) {
                    JOptionPane.showMessageDialog(frame,
                            "You have selected a different type of file. Please select a .txt file!",
                            "ERROR: NOT A .TXT FILE",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                try {
                    int firstLineIndex = Integer.parseInt(indexOfLine1TextField.getText());
                    int wordFromFirstLineIndex = Integer.parseInt(indexOfWord1TextField.getText());
                    int secondLineIndex = Integer.parseInt(indexOfLine2TextField.getText());
                    int wordFromSecondLineIndex = Integer.parseInt(indexOfWord2TextField.getText());
                    fileManage.switchWords(path, firstLineIndex, wordFromFirstLineIndex,
                            secondLineIndex, wordFromSecondLineIndex);
                    JOptionPane.showMessageDialog(frame,
                            String.format("The file was successfully saved! " +
                                    "Changed word at line %d and index %d with word at line %d and index %d!",
                                    firstLineIndex, wordFromFirstLineIndex, secondLineIndex, wordFromSecondLineIndex),
                            "CHANGED WORDS", JOptionPane.INFORMATION_MESSAGE);
                    actionStatus.setText("The file was successfully saved!");
                } catch (FileNotFoundException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: FILE NOT FOUND!", JOptionPane.ERROR_MESSAGE);
                } catch (EmptyFileException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: FILE IS EMPTY!", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException exception) {
                    JOptionPane.showMessageDialog(frame,
                            "File not loaded!", "ERROR: FILE NOT LOADED", JOptionPane.ERROR_MESSAGE);
                } catch (SwitchLinesException | SwitchWordsException exception) {
                    JOptionPane.showMessageDialog(frame,
                            exception.getMessage(), "ERROR: SWAP WORDS", JOptionPane.ERROR_MESSAGE);
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(frame,
                            "You should enter integer value! " + exception.getMessage(),
                            "ERROR: INVALID INPUT", JOptionPane.ERROR_MESSAGE);
                } catch (IOException exception) {
                    JOptionPane.showMessageDialog(frame, exception.getMessage(),
                            "ERROR: IOException", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}
